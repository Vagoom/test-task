<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ganga</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <h2>Logo</h2>
            </div>
            <div class="col-md-10">
                <ul id="navbar">
                    <li><a href="#">KONTAKTI</a></li>
                    <li><a href="#">KONSULTĀCIJAS</a></li>
                    <li><a href="#">APMĀCĪBAS</a></li>
                    <li><a href="#">PUBLIKĀCIJAS</a></li>
                    <li><a href="#">PAR AUTORU</a></li>
                    <li><a href="#">YOUTUBE</a></li>
                </ul>
                <br>
            </div>
        </div>
        <div class="row">
            <h2 id="content_name" style="text-align: center; color: #ffc34d;">KONTAKTI</h2>
        </div>
        <div class="row">
            <div class="col-md-2">
                <p>Foto</p>
                <br>
                <p>Seko mums</p>
            </div>
            <div class="col-md-10">
                <p style="text-align:justify">
                Eptiur? Ignam volupti onectata eatisit iundam harupta aboremporae nihil ilitiscia dest, volupta tenisimodit acepe
                quiandaerume pla ipsande ntionestent omnis ipsum re volupita nes es soluptae non rerchit ad magnim velibus
                inverundae nim qui repe perferum cus, oditatem que volupta temporest, eosantemquia corat reptaes eaturio
                nseque modi ut estio. Ugia estecep eriores sitatur?
                Ita poriberspid molore, es sit dolupisquiam fugia dellectatem autatur sus.
                Ario cus suntur sant aut faccaeriatet et endi di vendit plaboriore ni quam vel int illendae is aut ut eum expeles
                truptia cum ad moluptatur aliqui verum quiam eate siti consequidita cullendis dest, te et, vit ipsuntiorro cor sequi
                seque volest invero omnistiorero molupta testia adi accustis dolorro doloris eumquiat quis nobitas dus.
                Eptiur? Ignam volupti onectata eatisit iundam harupta aboremporae nihil ilitiscia dest, volupta tenisimodit acepe
                quiandaerume pla ipsande ntionestent omnis ipsum re volupita nes es soluptae non rerchit ad magnim velibus
                inverundae nim qui repe perferum cus, oditatem que volupta temporest, eosantemquia corat reptaes eaturio
                nseque modi ut estio. Ugia estecep eriores sitatur?
                Eptiur? Ignam volupti onectata eatisit iundam harupta aboremporae nihil ilitiscia dest, volupta tenisimodit acepe
                quiandaerume pla ipsande ntionestent omnis ipsum re volupita nes es soluptae non rerchit ad magnim velibus
                inverundae nim qui repe perferum cus, oditatem que volupta temporest, eosantemquia corat reptaes eaturio
                </p>
            </div>
        </div>
        <hr>
        <div class="row">
            <form id="contact_form" action="post">
                <div class="col-md-6">
                    <input type="text" name="firstname" placeholder="Vārds:">
                    <input type="text" name="lastname" placeholder="Uzvārds">
                    <input type="text" name="email" placeholder="Epasts:">
                </div>
                <div class="col-md-6">
                    <textarea placeholder="Teksts:"></textarea>
                </div>
                <div class="col-md-12">
                    <input type="submit" value="SŪTĪT">
                </div>
            </form>
        </div>
    </div>

</body>
</html>